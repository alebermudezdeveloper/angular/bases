
const skills: string[] = ['Bash', 'Counter', 'Healing'];

interface CharacterInteface {
  name: string;
  hp: number;
  skills: string[];
  homeTown?: string;
}

const strider: CharacterInteface = {
  name: 'Strider',
  hp: 100,
  skills: ['Bash', 'Counter']
}

strider.homeTown = 'Rivendell';

console.table(strider);

export {}