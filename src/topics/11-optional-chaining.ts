
export interface Passenger {
  name: string;
  children?: string[];
}

const passenger1: Passenger = {
  name: "Alejandro"
}

const passenger2: Passenger = {
  name: "Vivian",
  children: ['Fernando', 'Felipe']
}

const returnChildrenNumber = ( passenger: Passenger): number => {

  if ( !passenger.children ) return 0;

  // const howManyChildren = passenger.children?.length || 0;
  const howManyChildren = passenger.children!.length || 0;

  console.log(passenger.name, howManyChildren);

  return howManyChildren;
}

returnChildrenNumber( passenger1 );