
interface AudioPlayer {
  audioVolume: number;
  songDuration: number;
  song: string;
  details: Details;
}

interface Details {
  author: string;
  year: number
}

const audioPlayer: AudioPlayer = {
  audioVolume: 90,
  songDuration: 36,
  song: "One",
  details: {
    author: "Metallica",
    year: 1992
  }
}

const song = 'New Song';

const { song:anotherSong, songDuration:duration, details } = audioPlayer;
const { author } = details;

console.log('Song:', song);
console.log('Song 2:', anotherSong);
console.log('Duration:', duration);
console.log('Author:', author);

const [p1, p2, trunks = 'Not Found']: string[] = ['Goku', 'Vegeta'];

console.log('Personaje 3:', trunks);

export {}