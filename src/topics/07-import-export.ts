import { Product, taxCalculation } from "./06-function-destructuring";


const shoppingCart: Product[] = [
  {
    description: 'Nokia',
    price: 100
  },
  {
    description: 'iPad',
    price: 150
  }
];

const tax: number = 0.15;

const [ total, taxTotal ]: number[] = taxCalculation({products: shoppingCart, tax});

console.log('Imported Total', total);
console.log('Imported Tax', taxTotal);