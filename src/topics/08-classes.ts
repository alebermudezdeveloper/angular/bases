
export class Person {

  constructor( 
    public firstName: string, 
    public lastName: string, 
    private address: string = 'No Address'
  ) {}

}

export class Hero extends Person {

  constructor(
    public alterEgo: string,
    public age: number,
    public realName: string,
  ) {
    super(realName, 'New York')
  }

}

export class Hero2 {
  constructor(
    public alterEgo: string,
    public age: number,
    public person: Person,
  ) { }

}

const tony = new Person('Tony', 'Stark', 'New York');

const ironman = new Hero2('IronMan', 45, tony);

console.log(ironman);