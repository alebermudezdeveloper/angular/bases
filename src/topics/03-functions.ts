interface CharacterInteface {
  name: string;
  hp: number;
  showHp: () => void;
}


function addNumbers(number1: number, number2: number): number {
  return number1 + number2;
}

const addNumbersArrow = (number1: number, number2: number): string => {
  return `${number1 + number2}`
}

function multiply(number1: number, number2?: number, base: number = 2) {
  return number1 * base;
}

const healCharacter = (character: CharacterInteface, amount: number) => {
  character.hp += amount;
}

const strider: CharacterInteface = {
  name: 'Strider',
  hp: 50,
  showHp() {
    console.log(`Health Points ${this.hp}`);
  }
}

healCharacter(strider, 10);

strider.showHp();

// const result: number = addNumbers(1, 2);
// const resultArrow: string = addNumbersArrow(4, 2);
// const resultMultiply: number = multiply(5);
// console.log({result, resultArrow, resultMultiply});

export {}